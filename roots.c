#include <math.h>
#include <stdio.h>
void input(int *a,int *b,int *c){
    printf("Enter the value of a,b,c in expression ax^2+bx+c\n");
    scanf("%d%d%d",a,b,c);
}

void compute(int t,int y,int u,float *r1,float *r2,float *d){
    *d=y*y-4*t*u;
    if(*d>0){
        *r1=(-y + sqrt(*d))/2*t;
        *r2=(-y - sqrt(*d))/2*t;
    }
    else if(*d==0){
        *r1=-y/2*t;
        *r2=*r1;
    }
}
void output(int d, float r1,float r2){
    if(d>0)
        d=1;
    else if(d==0)
        d=0;
    switch(d){
        case 1: printf("The roots are = %f %f",r1,r2);
        break;
        case 0: printf("The roots are = %f %f",r1,r2);
        break;
        default : printf("Imaginary Roots\n");
    }
}
int main()
{
    int a,b,c;float r1,r2;float d;
    input(&a,&b,&c);
    compute(a,b,c,&r1,&r2,&d);
    output(d,r1,r2);
    return 0;
}
