//Develop a C program to read and print employee details using structures
#include <stdio.h>
#define n 2000
struct Dob{
    int d,m,y;
};
struct details{
    char name[n];
    char gen;
    long pno;
    int employee_id;
    struct Dob dob;
    char ms;
    char bg;
    int ex;
    char coun[n];
};
typedef struct details empd;
void input(empd *emp){
    printf("Enter employee's name\n");
    fgets(emp->name,n,stdin);
    printf("Enter the employee id no\n");
    scanf("%d",&emp->employee_id);
    printf("Enter the gender as M or F or O\n");
    scanf(" %c", &emp->gen);
    printf("Enter date of birth as dd/mm/yyyy\n");
    scanf("%d/%d/%d",&emp->dob.d,&emp->dob.m,&emp->dob.y);
    scanf("%*c");
    printf("Country You belong\n");
    fgets(emp->coun,n,stdin);
    printf("Enter blood group\n");
    scanf(" %c", &emp->bg);
    printf("Enter marital status as s for single and m for married\n");
    scanf(" %c", &emp->ms);
    printf("Enter the years of experience\n");
    scanf("%d",&emp->ex);
    printf("Your contact no\n");
    scanf("%ld",&emp->pno);
    printf("Thank you!!\n\n");
}
void output(empd emp){
    printf("Name : ");
    puts(emp.name);
    printf("Id : %d",emp.employee_id);
    printf("\nGender : %c",emp.gen);
    printf("\nDate of birth : %d/%d/%d",emp.dob.d,emp.dob.m,emp.dob.y);
    printf("\nCountry : ");
    puts(emp.coun);
    printf("Blood group : %c",emp.bg);
    printf("\nStatus : %c",emp.ms);
    printf("\nExperience : %d",emp.ex);
    printf("\ncontact details : %ld\n\n\n",emp.pno);
}
int main(){
    empd emp;
    input(&emp);
    output(emp);
    return 0;
}
