#include <stdio.h>
int fact(int n){
    int fac=1;
    while(n!=0){
        fac*=n;
        n--;
    }
    return fac;
}
int main(void){
    int n;
    float s=0.0;
    printf("Enter the number of terms\n");
    scanf("%d",&n);
    for(int i=1;i<=n;i++)
        s+=(float)i/fact(i);
    printf("The sum is = %0.2f",s);
}