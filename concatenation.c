//string concatination

#include<stdio.h>
void input(char s[]){
    printf("Enter the string\n");
    scanf("%s",s);
}

void concat(char s1[],char s2[]){
    int i=0,j=0;
    for(i=0;s1[i]!='\0';i++);
    while(s2[j]!='\0'){
        s1[i]=s2[j];
        j++;i++;
    }
    s1[i]='\0';
}

void output(char s[]){
    printf("the string is %s",s);
}

int main(void){
    char s1[100],s2[100];
    input(s1);
    input(s2);
    concat(s1,s2);
    output(s1);
}