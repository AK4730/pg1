#include<stdio.h>
#include<math.h>
float sumf(float, float);
int sumi(int, int);
int sq(int);
float sumf(float f1,float f2)
{
    return f1+f2;
}
int sumi(int i1, int i2)
{
    return i1+i2;
}
int sq(int a)
{
    return pow(a,2);
}
int main()
{
    int num1,num2,po;
    float dec1,dec2;
    printf("Enter integers to be added\n");
    scanf("%d %d",&num1,&num2);
    printf("Enter two decimal numbers\n");
    scanf("%f %f",&dec1,&dec2);
    printf("Enter integer to be squared\n");
    scanf("%d",&po);
    printf("sum of integers = %d\n",sumi(num1,num2));
    printf("sum of decimals = %0.2f\n",(float)sumf(dec1,dec2));
    printf("sqared number = %d\n",sq(po));
    return 0;
}