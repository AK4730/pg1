#include <stdio.h>

void input(int *a){
    printf("Enter the Year\n");
    scanf("%d",a);
}
void cal(int x){
    if(x%400==0)
        printf("It's a leap year\n");
    else if(x%4==0 && x%100!=0)
        printf("It's a leap year\n");
    else
        printf("It's not\n");
}
int main(){
    int l;
    input(&l);
    cal(l);
    return 0;
}