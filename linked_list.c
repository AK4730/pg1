#include <stdio.h>
#include <stdlib.h>
struct node{
    int a;
    struct node *next;
};
void printList(struct node *n) 
{ 
    while (n != NULL) { 
        printf(" %d ", n->a); 
        n = n->next; 
    } 
} 
  
int main()
{
    struct node *head = NULL;
    struct node *second = NULL;
    struct node *first = NULL;
    head = (struct node*)malloc(sizeof(struct node));
    first = (struct node*)malloc(sizeof(struct node));
    second = (struct node*)malloc(sizeof(struct node));
    head->a=1;
    head->next=second;
    second->a=2;
    second->next=first;
    first->a=5;
    first->next=NULL;
    printList(head);
    
    return 0;
}