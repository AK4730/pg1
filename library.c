#include <stdio.h>
struct date{
    int dd,mm,yy;
};
struct book{
    char name[500],author[500];
    struct date dop;
    int price;
    long int isbn;
};
int main(void){
    struct book b;
    printf("Enter the name of the book\n");
    scanf("%s",b.name);
    printf("Enter the author\n");
    scanf("%s",b.author);
    printf("Enter the date of publishing\n");
    scanf("%d%*c%d%*c%d",&b.dop.dd,&b.dop.mm,&b.dop.yy);
    printf("Enter Price\n");
    scanf("%d",&b.price);
    printf("Enter ISBN\n");
    scanf("%ld",&b.isbn);
    printf("\n\n");
    printf("Name of the book = %s\n",b.name);
    printf("Author = %s\n",b.author);
    printf("price = %d\n",b.price);
    printf("Date of publishing = %d/%d/%d\n",b.dop.dd,b.dop.mm,b.dop.yy);
    printf("ISBN = %ld",b.isbn);
}