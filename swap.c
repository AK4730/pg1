#include<stdio.h>
int input(){
    int a;
    printf("Enter the numbers\n");
    scanf("%d",&a);
    return a;
}

//when the values are changed 
void swap(int *a,int *b){
    *a=*a+*b;
    *b=*a-*b;
    *a=*a-*b;
}

void output(int a,int b){
    printf("value after swapping\n");
    printf("%d %d",a,b);
}

int main(void){
    int a,b;
    a=input();
    b=input();
    swap(&a,&b);
    output(a,b);
}