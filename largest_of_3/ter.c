#include <stdio.h>
int cal(int,int,int);
void input(int *);
void output(int );
void input(int *a){
    printf("Enter the number\n");
    scanf("%d",a);
}
int cal(int x,int y,int z){
    int c;
    c=(x>=y)? ((x>=z)?x : z) : ((y>=z)?y : z);
    return c;
}
void output(int b){
    printf("\n%d is largest\n",b);
}
int main()
{
    int p,q,r,c;
    input(&p);
    input(&q);
    input(&r);
    c=cal(p,q,r);
    output(c);
    return 0;
}