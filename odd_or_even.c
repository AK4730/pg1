#include <stdio.h>

void input(int *a){
    printf("Enter the Number\n");
    scanf("%d",a);
}
int cal(int x){
    if((x/2)*2==x)
        return 1;
    else
        return 2;
}
void output(int z){
    if(z==1)
        printf("The number is even\n");
    else
        printf("The number is odd\n");
}
int main(){
    int l,m;
    input(&l);
    m=cal(l);
    output(m);
    return 0;
}