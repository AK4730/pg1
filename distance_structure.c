#include <stdio.h>
#include <math.h>
struct point
{
    float x,y;
};
float dis(float,float,float,float);
float dis(float a,float b,float c,float d)
{
    float dist;
    dist= sqrt(pow((a-c),2)+pow((b-d),2));
    return dist;
}
int main()
{
    struct point p1,p2;
    printf("Enter the first coordinate\n");
    scanf("%f %f",&p1.x,&p1.y);
    printf("Enter the second coordinate\n");
    scanf("%f %f",&p2.x,&p2.y);
    printf("your distance = %0.2f",dis(p1.x,p1.y,p2.x,p2.y));
    return 0;
}