#include <stdio.h>
int main(){
    int n,pos=0,neg=0,z=0,sump=0,sumn=0;
    float avgp,avgn;
    printf("Enter the number\n");
    scanf("%d",&n);
    while(n!=-1){
        if(n>0){
            pos++;
            sump+=n;
        }
        else if(n<0){
            neg++;
            sumn+=(-n);
        }
        else z++;
        printf("Enter another number\n");
        scanf("%d",&n);
    }
    avgp=(float)sump/pos;
    avgn=(float)sumn/neg;
    printf("No of positive terms = %d\n",pos);
    printf("Sum = %d Average = %0.2f\n",sump,avgp);
    printf("No of Negative terms = %d\n",neg);
    printf("Sum = -%d Average = -%0.2f\n",sumn,avgn);
    printf("No. of Zeros = %d\n",z);
    return 0;
}